# Hier stehen alle Umgebungsvariablen (Environment), die bei jedem Start der zsh und vor allen anderen aufgerufen werden.

# XDG-Standard
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state
export XDG_DATA_DIRS=/usr/local/share:/usr/share
export XDG_CONFIG_DIRS=/etc/xdg

# Zsh-Heimat
export ZDOTDIR=$XDG_CONFIG_HOME/zsh
export HISTFILE=$XDG_STATE_HOME/zsh/history

# Default-Programme, die ich setze

#if [ -n "$DISPLAY" ];
#then 
    export READER=zathura
    export TERMINAL=kitty
    export BROWSER=firefox
#else
#    export READER=/usr/bin/fbgs
#    export BROWSER=/usr/bin/w3m
#fi

export PAGER=/usr/bin/less
# Ursprünglich für Editoren wie vi, heute manchmal auch GUI
export VISUAL=/usr/bin/vim 
# Ursprünglich für Editoren wie ex (vi -e), heute manchmal auch für TUI
export EDITOR=/usr/bin/vim 

# GTK Variablen
## Input
export GTK_IM_MODULE=fcitx
## Configs
export GTK2_RC_FILES=$XDG_CONFIG_HOME/gtk-2.0/gtkrc
export GTK_RC_FILES=$XDG_CONFIG_HOME/gtk-1.0/gtkrc

# QT-Variablen
## Platformtheming
export QT_IM_MODULE=fcitx
## Abstraction
export QT_QPA_PLATFORM=wayland-egl
## Theming
export QT_STYLE_OVERRIDE=kvantum

# Legacy X11
export XINITRC=$XDG_CONFIG_HOME/X11/xinitrc
export XSERVERRC=$XDG_CONFIG_HOME/X11/xserverrc
export USERXSESSION=$XDG_CACHE_HOME/X11/xsession
export USERXSESSIONRC=$XDG_CACHE_HOME/X11/xsessionrc
export ALTUSERXSESSION=$XDG_CACHE_HOME/X11/Xsession
export ERRFILE=$XDG_CACHE_HOME/X11/xsession-errors
# Probleme mit LightDM
export XAUTHORITY=$XDG_RUNTIME_DIR/Xauthority

# Dotfiles
export GNUPGHOME=$XDG_CONFIG_HOME/gnupg
export LESSHISTFILE=$XDG_CONFIG_HOME/less/lesshst
export LESSKEY=$XDG_CONFIG_HOME/less/keys
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export PARALLEL_HOME=$XDG_CONFIG_HOME/parallel
export LYNX_CFG=$XDG_CONFIG_HOME/lynxrc
export PYTHONSTARTUP=$XDG_CONFIG_HOME/python/pythonstartup
export WGETRC=$XDG_CONFIG_HOME/wgetrc
export TEXMFCONFIG=$XDG_CONFIG_HOME/texlive/texmf-config

# Data-Files
export CARGO_HOME=${XDG_DATA_HOME:-${HOME/.local/share}}/cargo
export RUSTUP_HOME=$XDG_DATA_HOME/rustup 
export WINEPREFIX=$XDG_DATA_HOME/wineprefixes/default
export SSB_HOME=$XDG_DATA_HOME/zoom
export PASSWORD_STORE_DIR=$XDG_DATA_HOME/pass
export TEXMFHOME=$XDG_DATA_HOME/texmf

# Caching
export TEXMFVAR=$XDG_CACHE_HOME/texlive/texmf-var

# Anderer Kram
## Input für Xorg (überflüssig?)
export XMODIFIERS=@im=fcitx
## Input für SDL2 (überflüssig?)
export SDL_IM_MODULE=fcitx
## Input kitty – läuft nicht richtig. Vielleicht ein Problem mit Sway/kitty/fcitx5?
export GLFW_IM_MODULE=ibus
## Wayland für Mozilla
export MOZ_ENABLE_WAYLAND=1 
## Ranger .conf laden
export RANGER_LOAD_DEFAULT_RC=FALSE 
