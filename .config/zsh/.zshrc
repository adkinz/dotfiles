# $HOME/.config/zsh/.zshrc
# Meine zshrc. Benutzung auf eigene Gefahr!
#
# Maintainer:	Adrian Kinzig <adkinz123@aol.de>
# Last change:	2022 Aug 23
#
# Anmerkungen für mich:
    # "zstyle" dient zum Einbinden verschiedener Programme und Dateien. Es gilt:
    # zstyle ':completion:function:completer:command:argument:tag' style values
    #
    # "source" lädt die nachfolgende Datei an Ort und Stelle


########### Hier der Hauptcode ############
    # Standardsachen
    source $ZDOTDIR/aliases	# Lade Aliase
    #export PATH="$PATH":"$(du $HOME/.local/bin/ | cut -f2 | tr '\n' ':' | sed 's/:*$//')"  # ~/.local/bin/* zu PATH hinzufügen
    # Roswell in Path einbinden
    export PATH=$PATH:$HOME/.roswell/bin
    source /usr/share/zsh/plugins/zsh-vi-mode/zsh-vi-mode.zsh # Besserer VI-MODE

    # Farben
    autoload -Uz colors && colors   # Farben laden
     
    # History
    HISTSIZE=50000		# Größe der History
    SAVEHIST=10000		# Eintragsmenge in der History
    setopt HIST_REDUCE_BLANKS	# Überflüssige Leerzeichen löschen
    setopt HIST_IGNORE_ALL_DUPS # Ältere identische Befehle löschen

    # Autovervollständigung
    # Aka. sämtlicher Code, der mit "comp-" beginnt
    zstyle :compinstall filename '$ZDOTDIR/.zshrc'	    # Ort für die Konfiguration (?)
    autoload -Uz compinit				    # Funktion Autovervollständigung laden
    compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION  # Fuktion aufrufen mit Ort für den Cache
    zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache	# Mehr Cache :D
    source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh	# Code laden für fish-ähnliche Vervollständigung
    zmodload zsh/complist				    # 
    setopt MENU_COMPLETE				    # Menüauswahl bei Ergänzung per Flag
    setopt COMPLETE_ALIASES				    # Aliase wie Befehle behandeln
    zstyle ':completion:*' menu select			    # Menüfunktion bei der Auswahl von Befehlen
    zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'   # Halbe Namen vervollständigen

    # Versionen-Control-System (zur Überwachung von git)
    autoload -Uz vcs_info				# Funktion für das VCS laden
    precmd() vcs_info					# Funktion vor dem zeichnen des Prompts nutzen
    zstyle ':vcs_info:*' check-for-changes true		# Nach (un)staged checken, für %u und %c
    zstyle ':vcs_info:*' unstagedstr ' *'		# String für unstaged Veränderungen (*)
    zstyle ':vcs_info:*' stagedstr ' +'			# String für staged Veränderungen (+)
    zstyle ':vcs_info:git:*' formats       '(%b%u%c)'	# Format für vcs_info
    zstyle ':vcs_info:git:*' actionformats '(%b|%a%u%c)' # Format für vcs_info 

    # Promptanpassung
    bindkey $terminfo[kich1] overwrite-mode	# Bearbeite den Terminfo-Insertkey (?)
    setopt PROMPT_SUBST				# Rechenoperationen können im Prompt durchgeführt werden
    source $ZDOTDIR/prompt			# Lade die Funktionen
    
    # Syntax-Highlighting
    # Muss immer am Schluss stehen
    source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh # Für Suche in der History
    bindkey '^[[A' history-substring-search-up
    bindkey '^[[B' history-substring-search-down

    ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern) # Es ist auch noch möglich cursor zu laden
