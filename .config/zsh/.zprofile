# Diese Datei wird von der Zshell nur beim Login nach .zshenv und vor .zshrc gelesen

# Starte Sway, wenn von TTY1 aus eingelogt wird
if [ "$(tty)" = "/dev/tty1" ]; then
   exec sway
fi
