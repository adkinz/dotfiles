" $RTP/vimrc.d/keymaps.vim
"
" Leader
    let mapleader="ö"           " Change Leader from \ to ö
    let maplocalleader="ö"	" Localleader as ö

" Normal-Mode
    "Create empty line above
    nnoremap <leader>o :call append(line('.'), '')<CR>
    "Create empty line below
    nnoremap <leader>O :call append(line('.')-1, '')<CR>
    " Deactivate highlighting for search
    nnoremap <leader>h :noh<CR>

    " Templates
    " tex-scrreprt
    nnoremap <leader>tex :-1read $RTP/vorlagen/latex.tex<CR>110ggci{	
    " tex-scrbook
    nnoremap <leader>book :-1read $RTP/vorlagen/scrbook.tex<CR>257ggci{

    " Windows and Buffers
    " Open Netrw on the left side with ä
    nnoremap ä :Lexplore<CR>
    " go to file and open it in a new buffer
    nnoremap <leader>gf <cr>

" Insert-Modus


" Visual-Modus
