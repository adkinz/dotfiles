" Start a server for vimtex
    if empty(v:servername) && exists('*remote_startserver')
	call remote_startserver('VIM')
    endif

" Netrw - Bugs everywhere!
" Source: https://github.com/BrodieRobertson/dotfiles/blob/master/config/nvim/plugconfig/netrw.vim
   let  g:netrw_banner=0	    " Ignore Banner
   let  g:netrw_liststyle=3	    " Tree-View !very! buggy
   let  g:netrw_browse_split=4	    " Don't open Netrw again
   let  g:netrw_winsize=15	    " Size of the window
   let  g:netrw_altv=1		    " window on the right
   let  g:netrw_use_errorwindow=1   " Errors out of the way
				    " Close Netrw if its the last window
   aug netrw_close
	au!
	au WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == "netrw"|q|endif
   aug END
   
" YouCompleteMe-Plugin
    let g:ycm_max_num_candidates = 10	    " Zahl der Vorschläge
    if !exists('g:ycm_semantic_triggers')   " checkt ob Triggervariable gesetzt wurde
      let g:ycm_semantic_triggers = {}	    " wenn nicht, Variable setzen
    endif

