" $HOME/.vim/ftplugin/bib.vim
"
if filereadable(expand('$RTP/ftplugin/tex.vim'))
    runtime ftplugin/tex.vim
endif  

" Vimtex for Biblatex
let g:vimtex_indent_bib_enabled = 1
let g:vimtex_complete_enabled = 1 
let g:vimtex_fold_bib_enabled = 1
