" $HOME/.vim/ftplugin/tex.vim
"
" Activate autocorrect
setlocal spell
" call pencil#init()
" call textobj#sentence#init()	

" Vimtex
let g:tex_flavor = 'latex'
let g:vimtex_view_method='zathura'
let g:vimtex_indent_enabled = 1
let g:vimtex_view_automatic = 1
let g:vimtex_complete_enabled = 1
let g:vimtex_complete_close_braces = 1
let g:vimtex_complete_ignore_case = 1
let g:vimtex_complete_smart_case = 1
let g:vimtex_fold_enabled = 1
let g:vimtex_fold_types_default = 1
let g:vimtex_syntax_conceal = {
  \ 'spacing': 0,
  \ 'accents': 1,
  \ 'ligatures': 1,
  \ 'cites': 1,
  \ 'fancy': 1,
  \ 'greek': 1,
  \ 'math_bounds': 1,
  \ 'math_delimiters': 1,
  \ 'math_fracs': 1,
  \ 'math_super_sub': 1,
  \ 'math_symbols': 1,
  \ 'sections': 1,
  \ 'styles': 1,
  \}
let g:vimtex_compiler_latexmk = { 
	    \ 'build_dir' : 'build_dir',
	    \ 'callback' : 1,
	    \ 'continuous' : 1,
	    \ 'executable' : 'latexmk',
	    \ 'options' : [
	    \ '-verbose', 
	    \ '-file-line-error', 
	    \ '-synctex=1', 
	    \ '-interaction=nonstopmode',  
	    \ ], 
	    \}

" Check in vimrc.d/spell.vim
  if !exists('g:ycm_semantic_triggers')
    let g:ycm_semantic_triggers = {}
  endif
  au VimEnter * let g:ycm_semantic_triggers.tex=g:vimtex#re#youcompleteme

let g:ycm_complete_in_comments = 1
